const express = require("express");
const gameController = require("../controllers/gameController");

const router = express.Router();

//--------RENDER PAGE HOME----------//
router.get("/", gameController.render_home);

//--------RENDER PAGE GAME----------//
router.get("/game", gameController.render_game);

//---------GET ALL DATA USER-------//
router.get("/users", gameController.get_data_user);

//--------RENDER LOGIN----------//
router.get("/login", gameController.render_login);

module.exports = router;
