const userData = require("../data/dummyData");

// --------------RENDER PAGE HOME --------//

const render_home = (req, res) => {
  res.render("home");
};

//----------------RENDER PAGE GAME --------//
const render_game = (req, res) => {
  res.render("game");
};

//----------------GET DATA USERS ---------//
const get_data_user = (req, res) => {
  res.json(userData);
};

const render_login = (req, res) => {
  res.render("login");
};

module.exports = {
  render_home,
  render_game,
  get_data_user,
  render_login,
};
