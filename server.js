const express = require("express");
const morgan = require("morgan");
const gameRoutes = require("./routes/gameRoutes");

const app = express();
const port = 3000;

app.listen(port, () => {
  console.log(`Server started on ${port}`);
});

//------------------------VIEW ENGINE EJS----------------------//
app.set("view engine", "ejs");

//-------------------------MIDDLEWARE--------------------------//
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));

//---------------------------ROUTES----------------------------//
app.use("/", gameRoutes);

//-----------------------------404-----------------------------//
app.use((req, res) => {
  res.status(404).render("404");
});
