const options = document.querySelectorAll(".user-choice");
let userChoice;
let computerChoice;
let result;

options.forEach((option) =>
  option.addEventListener("click", (e) => {
    userChoice = e.target.id;
    PlayerComputer.generateCompChoice();

    console.log(
      `User memilih ${userChoice} dan Computer memilih ${PlayerComputer.generateCompChoice()}`
    );
    getResult();
  })
);

//---------Parent Class (abstract)----------
class Player {
  constructor(nama) {
    nama: nama;
  }
}

const Comp = (Base) =>
  class extends Base {
    generateCompChoice() {
      const compChoice = document.querySelectorAll(".comp-choice");

      const randomNumber = Math.floor(Math.random() * compChoice.length);
      // console.log(randomNumber);

      if (randomNumber === 0) {
        computerChoice = "rock";
      }
      if (randomNumber === 1) {
        computerChoice = "paper";
      }
      if (randomNumber === 2) {
        computerChoice = "scissors";
      }

      // console.log(`fungsi generate : ${computerChoice}`);
      return computerChoice;
    }
  };

//-------------- Sub Class -------------
class PlayerComp extends Comp(Player) {
  constructor(props) {
    super(props);
  }
  generateChoice() {
    super.generateCompChoice();
  }
}

//--------- object Computer User ---------------
const PlayerComputer = new PlayerComp({
  nama: "Computer",
});

//---------game -------

function getResult() {
  if (computerChoice === userChoice) {
    result = "Draw";
  } else if (computerChoice === "rock" && userChoice === "scissors") {
    result = "Computer Win";
  } else if (computerChoice === "paper" && userChoice === "rock") {
    result = "Computer Win";
  } else if (computerChoice === "paper" && userChoice === "scissors") {
    result = "User Win";
  } else if (computerChoice === "scissors" && userChoice === "rock") {
    result = "User Win";
  } else if (computerChoice === "scissors" && userChoice === "paper") {
    result = "Computer Win";
  } else if (computerChoice === "scissors" && userChoice === "paper") {
    result = "Computer Win";
  }
  console.log(result);
}
